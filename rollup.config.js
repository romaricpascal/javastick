import { babel } from '@rollup/plugin-babel';
import { terser } from 'rollup-plugin-terser';
import pkg from './package.json';

export default [
  // An ESM build that can be loaded in the browser with `<script type="module">`
  // or used through your favourite bundler
  { input: 'src/index.js', output: [{ file: pkg.module, format: 'es' }] },
  // A minified ESM build to make loading quicker for `<script type="module">`
  {
    input: 'src/index.js',
    output: [
      {
        file: pkg.module.replace('.js', '.min.js'),
        format: 'es',
        sourcemap: true,
      },
    ],
    plugins: [terser()],
  },

  // An ES5 UMD build for supporting older browsers
  {
    input: 'src/browser.js',
    output: {
      name: 'javastick',
      file: pkg.browser.replace('umd', 'es5'),
      format: 'umd',
    },
    plugins: [babel({ babelHelpers: 'bundled' })],
  },
  // And its minified counterpart
  {
    input: 'src/browser.js',
    output: {
      name: 'javastick',
      file: pkg.browser.replace('umd', 'es5').replace('.js', '.min.js'),
      format: 'umd',
      sourcemap: true,
    },
    plugins: [babel({ babelHelpers: 'bundled' }), terser()],
  },
];
