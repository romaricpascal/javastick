module.exports = {
  extends: ['eslint:recommended', 'plugin:prettier/recommended'],
  parserOptions: {
    ecmaVersion: 10,
    sourceType: 'module',
  },
  env: {
    browser: true,
    node: true,
    es6: true,
  },
  rules: {
    // Because the .prettierrc file is `.cjs`
    'prettier/prettier': ['error', require('./.prettierrc.cjs')],
    'no-shadow': ['error', { builtinGlobals: true, hoist: 'all', allow: [] }],
  },
  overrides: [
    {
      files: ['**/__tests__/**/*'],
      env: {
        browser: false,
      },
      rules: {
        // Tests will require dev only packages
        'node/no-unpublished-require': 0,
        // Allow tests to use a local the `node_modules`
        'node/no-extraneous-require': 0,
      },
    },
    {
      files: ['cypress/**/*.js', '**/*.spec.js'],
      env: {
        browser: false,
      },
      extends: ['plugin:cypress/recommended'],
    },
  ],
};
