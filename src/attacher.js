export function attacher({
  behaviors = {},
  behaviorsByElement = new WeakMap(),
  onMissingBehavior,
  ...extraBehaviors
} = {}) {
  Object.assign(behaviors, extraBehaviors);

  return {
    attach,
    detach,
  };

  function attach(element, behaviorNames) {
    const elementBehaviors = getBehaviors(element);

    const { additions, removals } = diff(
      Object.keys(elementBehaviors),
      behaviorNames,
    );

    removals.forEach((behaviorName) => {
      detachBehavior(element, behaviorName, elementBehaviors);
    });

    additions.forEach((behaviorName) => {
      attachBehavior(element, behaviorName, elementBehaviors);
    });
  }

  function detach(element) {
    const elementBehaviors = getBehaviors(element);
    for (const behaviorName of Object.keys(elementBehaviors)) {
      detachBehavior(element, behaviorName, elementBehaviors);
    }
  }

  function attachBehavior(element, behaviorName, elementBehaviors) {
    const behavior = behaviors[behaviorName];
    if (behavior) {
      elementBehaviors[behaviorName] = behavior(element);
    } else {
      if (onMissingBehavior) {
        onMissingBehavior(element, behaviorName, elementBehaviors);
      }
    }
  }

  function detachBehavior(element, behaviorName, elementBehaviors) {
    const teardown = elementBehaviors[behaviorName];
    if (teardown && typeof teardown == 'function') {
      teardown(element);
    }
    delete elementBehaviors[behaviorName];
    // Tidy up the list of attachments as the element
    // no longer has any
    if (!Object.keys(elementBehaviors).length) {
      behaviorsByElement.delete(element);
    }
  }

  function getBehaviors(element) {
    return (
      behaviorsByElement.get(element) ||
      (() => {
        const elementBehaviors = {};
        behaviorsByElement.set(element, elementBehaviors);
        return elementBehaviors;
      })()
    );
  }
}

export const MISSING_BEHAVIOR = Symbol('missing behavior');

export function updatableAttacher({
  behaviors = {},
  behaviorsByElement = new Map(),
  onMissingBehavior,
  ...options
} = {}) {
  return {
    ...attacher({
      behaviors,
      behaviorsByElement,
      onMissingBehavior: function _onMissingBehavior(
        element,
        behaviorName,
        elementBehaviors,
      ) {
        if (onMissingBehavior) {
          onMissingBehavior(element, behaviorName, elementBehaviors);
        }
        if (!elementBehaviors[behaviorName]) {
          elementBehaviors[behaviorName] = MISSING_BEHAVIOR;
        }
      },
      ...options,
    }),
    install(behaviorName, fn) {
      behaviors[behaviorName] = fn;

      for (const [element, elementBehaviors] of behaviorsByElement) {
        if (elementBehaviors[behaviorName] == MISSING_BEHAVIOR) {
          elementBehaviors[behaviorName] = fn(element);
        }
      }
    },
  };
}

function diff(existing, incoming) {
  return {
    additions: incoming.filter((value) => !existing.includes(value)),
    removals: existing.filter((value) => !incoming.includes(value)),
  };
}
