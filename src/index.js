import { updatableAttacher } from './attacher.js';
export * from './attacher.js';

export const DEFAULT_ATTRIBUTE = 'data-is';

/**
 * A preconfigured but configurable Javastick runtime
 * @param {*} [options]
 * @returns {*}
 */
export function javastick({
  // Options for detecting behaviors on elements
  attribute = DEFAULT_ATTRIBUTE,
  matcherOptions = { attribute },
  matcher = byAttribute,
  // Options for starting the observer
  root = document.documentElement,
  observerOptions = {},
  observer = elementObserver,
  // The behaviors themselves and how they'll get attached
  behaviors = {},
  onMissingBehavior,
  attacherOptions = {},
  attacher = updatableAttacher,
  // Handler for the elementObserver
  handlerOptions = {},
  handler = simpleHandler,
  // Convenience for starting automatically
  start = true,
  // Collect any other argument as behaviours for a nicer API
  ...extraBehaviors
} = {}) {
  const api = runtime({
    matcherOptions,
    matcher,
    attacherOptions: {
      behaviors: Object.assign(behaviors, extraBehaviors),
      onMissingBehavior,
      ...attacherOptions,
    },
    attacher,
    handlerOptions,
    handler,
    observerOptions: {
      root,
      attributeFilter: [attribute],
      ...observerOptions,
    },
    observer,
  });

  if (start) {
    api.start();
  }

  return api;
}

/**
 * @param {*} options
 * @returns {*}
 */
export function runtime({
  matcherOptions,
  matcher,
  attacherOptions,
  attacher,
  handlerOptions,
  handler,
  observerOptions,
  observer,
}) {
  if (typeof matcher == 'function') {
    matcher = matcher(matcherOptions);
  }
  if (typeof attacher == 'function') {
    attacher = attacher(attacherOptions);
  }

  if (typeof handler == 'function') {
    handler = handler({ attacher, matcher, ...handlerOptions });
  }

  const observerApi = observer({ handler, ...observerOptions });
  return {
    ...observerApi,
    attacher,
    handler,
    matcher,
    install(...args) {
      attacher.install(...args);
    },
  };
}

export function elementObserver({
  handler,
  root = document.documentElement,
  ...observerOptions
}) {
  // Monitor DOM changes to connect/disconnect behaviors
  const mutationObserver = new MutationObserver((mutationList) => {
    mutationList.forEach((mutation) => {
      if ('childList' === mutation.type) {
        // Only one mutation event will happen for
        // insertion/deletion of nested nodes so handling
        // will need to take care both of the node itself
        // and its descendants
        mutation.removedNodes.forEach((node) => {
          if (node.nodeType == Node.ELEMENT_NODE) {
            handler.onRemovedElement(node, mutation);
          }
        });
        mutation.addedNodes.forEach((node) => {
          if (node.nodeType == Node.ELEMENT_NODE) {
            handler.onAddedElement(node, mutation);
          }
        });
      }
      if ('attributes' === mutation.type) {
        handler.onAttributeChange(mutation.target, mutation);
      }
    });
  });

  return {
    start({ root: startRoot, observerOptions: startObserverOptions } = {}) {
      // Find all notes already present in the DOM and set them up
      handler.onAddedElement(startRoot || root);

      mutationObserver.observe(startRoot || root, {
        subtree: true,
        childList: true,
        attributes: true,
        ...(startObserverOptions || observerOptions),
      });
    },
    observer: mutationObserver,
  };
}

export function simpleHandler({ matcher, attacher }) {
  return {
    onRemovedElement,
    onAddedElement,
    onAttributeChange,
  };

  function onRemovedElement(element) {
    if (matcher.hasBehaviors(element)) {
      attacher.detach(element);
    }

    matcher
      .findDescendantsWithBehaviors(element)
      .forEach((descendant) => attacher.detach(descendant));
  }

  function onAttributeChange(element) {
    if (matcher.hasBehaviors(element)) {
      attacher.attach(element, matcher.getBehaviors(element));
    } else {
      attacher.detach(element);
    }
  }

  function onAddedElement(element) {
    // 1. Not only can the node itself have behaviors
    if (matcher.hasBehaviors(element)) {
      attacher.attach(element, matcher.getBehaviors(element));
    }

    // 2. But also of any of its descendants can
    matcher
      .findDescendantsWithBehaviors(element)
      .forEach((descendant) =>
        attacher.attach(descendant, matcher.getBehaviors(descendant)),
      );
  }
}

export function byAttribute({ attribute }) {
  return {
    getBehaviors(element) {
      return element.getAttribute(attribute).split(' ').filter(Boolean);
    },
    hasBehaviors(element) {
      return element.hasAttribute(attribute);
    },
    findDescendantsWithBehaviors(root) {
      return Array.prototype.slice.call(
        root.querySelectorAll(`[${attribute}]`),
      );
    },
  };
}
