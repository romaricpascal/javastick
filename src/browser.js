/**
 * A little rework of the exports to make the API accessible
 * through properties of the `javastick` function for easier
 * testing in the browser
 */
import * as api from './index.js';

const { javastick, ...helpers } = api;

Object.assign(javastick, helpers);

export default javastick;
