import { updatableAttacher } from '../../attacher.js';
import it from 'ava';
import Window from 'window';
import sinon from 'sinon';

it.beforeEach((t) => {
  const document = new Window().document;
  const element = document.createElement('div');
  const teardown = sinon.spy();

  t.context = {
    document,
    teardown,
    element,
    behaviors: {
      behavior: sinon.spy(),
      otherBehavior: sinon.spy(),
      toRemove: sinon.spy(() => teardown),
    },
  };
});

it('adds new behaviors to new elements', (t) => {
  const { element } = t.context;
  const subject = updatableAttacher();
  const toBeAdded = sinon.spy();

  subject.install('toBeAdded', toBeAdded);
  subject.attach(element, ['toBeAdded']);

  t.assert(toBeAdded.calledOnce);
});

it('adds new behaviors to existing elements', (t) => {
  const { element } = t.context;
  const subject = updatableAttacher();
  subject.attach(element, ['toBeAdded']);
  const toBeAdded = sinon.spy();

  subject.install('toBeAdded', toBeAdded);

  t.assert(toBeAdded.calledOnce);
});

it('calls provided `onMissingBehavior`', (t) => {
  const { element } = t.context;
  const onMissingBehavior = sinon.spy();
  const subject = updatableAttacher({ onMissingBehavior });

  subject.attach(element, ['unknown']);

  t.assert(onMissingBehavior.getCall(0).calledWith(element, 'unknown'));
});
