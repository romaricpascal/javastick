import { attacher } from '../../attacher.js';
import it from 'ava';
import Window from 'window';
import sinon from 'sinon';

it.beforeEach((t) => {
  const document = new Window().document;
  const element = document.createElement('div');
  const teardown = sinon.spy();

  t.context = {
    document,
    teardown,
    element,
    behaviors: {
      behavior: sinon.spy(),
      otherBehavior: sinon.spy(),
      toRemove: sinon.spy(() => teardown),
    },
  };
});

it('attaches the behavior to the node', (t) => {
  const { element, behaviors } = t.context;
  const subject = attacher({ behaviors });
  subject.attach(element, ['behavior']);
  t.assert(behaviors.behavior.calledOnce);
});

it('ignores unknown behaviors', (t) => {
  t.plan(0); // Test fails if an exception is thrown
  const { element, behaviors } = t.context;
  const subject = attacher({ behaviors });
  subject.attach(element, ['unknown']);
});

it('calls `onMissingBehavior` when trying to attach a missing behavior', (t) => {
  const { element } = t.context;
  const onMissingBehavior = sinon.spy();
  const subject = attacher({ onMissingBehavior });
  subject.attach(element, ['unknown']);
  t.assert(
    onMissingBehavior.getCall(0).calledWith(element, 'unknown'),
    'Called with the right arguments',
  );
});

it('attaches multiple behaviors to a node', (t) => {
  const { element, behaviors } = t.context;
  const subject = attacher({ behaviors });
  subject.attach(element, ['behavior', 'otherBehavior']);
  t.assert(behaviors.behavior.calledOnce);
  t.assert(behaviors.otherBehavior.calledOnce);
});

it('does not execute already attached behaviors', (t) => {
  const { element, behaviors } = t.context;
  const subject = attacher({ behaviors });
  subject.attach(element, ['behavior']);
  subject.attach(element, ['behavior']);
  t.assert(behaviors.behavior.calledOnce);
});

it('adds new behaviors to the element', (t) => {
  const { element, behaviors } = t.context;
  const subject = attacher({ behaviors });
  subject.attach(element, ['behavior']);
  subject.attach(element, ['behavior', 'otherBehavior']);
  t.assert(behaviors.behavior.calledOnce);
  t.assert(behaviors.otherBehavior.calledOnce);
});

it('removes behaviors from the element', (t) => {
  const { element, behaviors, teardown } = t.context;
  const subject = attacher({ behaviors });
  subject.attach(element, ['toRemove']);
  subject.attach(element, []);
  t.assert(teardown.calledOnce);
});

it('clears list of behaviors when last behavior is removed', (t) => {
  const { element, behaviors } = t.context;
  const behaviorsByElement = new WeakMap();
  const subject = attacher({ behaviors, behaviorsByElement });
  subject.attach(element, ['toRemove']);
  subject.attach(element, []);
  t.assert(!behaviorsByElement.has(element));
});

it('ignores non-function return values from behaviors', (t) => {
  // Aim is just to check that we don't throw
  // when a behavior returns something that's not a function
  t.plan(0);

  const { element } = t.context;

  // For this one, we need a special behavior
  // that'll return something, but not a function
  const behavior = sinon.spy(() => {
    return {};
  });
  const subject = attacher({
    behavior,
  });

  subject.attach(element, ['behavior']);
  subject.attach(element, []);
});

it('runs behaviors re-added after being removed', (t) => {
  const { element, behaviors } = t.context;
  const subject = attacher({ behaviors });
  subject.attach(element, ['toRemove']);
  subject.attach(element, []);
  subject.attach(element, ['toRemove']);
  t.is(behaviors.toRemove.callCount, 2);
});

it('detaches all behaviors of the element', (t) => {
  const { element, behaviors, teardown } = t.context;
  const behaviorsByElement = new WeakMap();
  const subject = attacher({ behaviors, behaviorsByElement });
  subject.attach(element, ['toRemove']);
  subject.detach(element);
  t.assert(teardown.calledOnce);
  t.assert(!behaviorsByElement.get(element));
});

it('ignores unknown elements', (t) => {
  t.plan(0); // Aim is just to check nothing throws
  const { document, element, behaviors } = t.context;
  const subject = attacher({ behaviors });
  subject.attach(element, ['toRemove']);
  subject.detach(document.createElement('div'));
});
