/* eslint-disable no-shadow */
import sinon from 'sinon';

context('Connect', () => {
  beforeEach(function () {
    cy.visit(`cypress/fixtures/blank.html`);
  });

  it(
    'Attaches the behaviour to nodes already on the page',
    withWindow(({ document, javastick }) => {
      document.body.innerHTML = '<div data-is="setup"></div>';
      const setup = Cypress.sinon.spy(
        (element) => (element.innerHTML = 'Setup'),
      );
      javastick({
        behaviors: {
          setup,
        },
      });

      expect(setup).to.have.been.called;
    }),
  );

  it(
    'Attaches the behaviour to nodes getting added to the page with `appendChild`',
    withWindow(async ({ document, javastick }) => {
      const setup = Cypress.sinon.spy(
        (element) => (element.innerHTML = 'Setup'),
      );
      javastick({
        behaviors: {
          setup,
        },
      });

      const div = document.createElement('div');
      div.setAttribute('data-is', 'setup');
      document.body.appendChild(div);
      // document.body.innerHTML = '<div data-is="setup"></div>';

      await afterMicrotask(() => expect(setup).to.have.been.called);
    }),
  );

  it(
    'Attaches the behaviour to nested nodes getting added to the page',
    withWindow(async ({ document, javastick }) => {
      const setup = Cypress.sinon.spy(
        (element) => (element.innerHTML = 'Setup'),
      );
      javastick({
        behaviors: {
          setup,
        },
      });

      document.body.innerHTML = `
      <div><div data-is="setup"></div></div>
      `;

      queueMicrotask(() => expect(setup).to.have.been.called);
    }),
  );

  it(
    'Attaches the behaviour to nodes whose attributes get added',
    withWindow(async ({ document, javastick }) => {
      const setup = Cypress.sinon.spy(
        (element) => (element.innerHTML = 'Setup'),
      );
      document.body.innerHTML = '<div></div>';
      javastick({
        behaviors: {
          setup,
        },
      });

      const div = document.querySelector('div');
      div.setAttribute('data-is', 'setup');

      await afterMicrotask(() => expect(setup).to.have.been.called);
    }),
  );

  it(
    'Attaches behaviours to nodes whose attributes get updated',
    withWindow(async ({ document, javastick }) => {
      const setup = Cypress.sinon.spy(
        (element) => (element.innerHTML = 'Setup'),
      );
      const later = Cypress.sinon.spy(
        (element) => (element.innerHTML = 'Later'),
      );
      document.body.innerHTML = '<div data-is="setup"></div>';
      javastick({
        behaviors: {
          setup,
          later,
        },
      });
      const div = document.querySelector('div');
      div.setAttribute('data-is', 'later');
      await afterMicrotask(() => {
        expect(later).to.have.been.called;
      });
    }),
  );

  it(
    'Detaches behaviours when it gets removed from list in attribute',
    withWindow(async ({ document, javastick }) => {
      const teardown = sinon.spy(
        () => (document.body.dataset.teardownCallCount = teardown.callCount),
      );

      document.body.innerHTML = `
        <div data-is="setup"></div>
      `;

      javastick({
        behaviors: {
          setup() {
            return teardown;
          },
        },
      });

      document.querySelector('[data-is]').setAttribute('data-is', '');

      await afterMicrotask(() => {
        expect(teardown).to.have.been.called;
      });
    }),
  );

  it(
    'Detaches behaviours when attribute gets removed',
    withWindow(async ({ document, javastick }) => {
      const teardown = sinon.spy(
        () => (document.body.dataset.teardownCallCount = teardown.callCount),
      );

      document.body.innerHTML = `
        <div data-is="setup"></div>
      `;

      javastick({
        behaviors: {
          setup() {
            return teardown;
          },
        },
      });

      document.querySelector('[data-is]').removeAttribute('data-is');

      await afterMicrotask(() => {
        expect(teardown).to.have.been.called;
      });
    }),
  );

  it(
    'Detaches behaviours when multiple nodes are removed from the page',
    withWindow(async ({ document, javastick }) => {
      // Set a little check in the DOM for debugging
      const teardown = sinon.spy(
        () => (document.body.textContent = teardown.callCount),
      );
      document.body.innerHTML = `
        <div data-is="toRemove"></div>
        <div>
          <div data-is="toRemove"></div>
        </div>
      `;
      javastick({
        behaviors: {
          toRemove() {
            return teardown;
          },
        },
      });

      document.body.innerHTML = '';
      await afterMicrotask(() => {
        expect(teardown).to.have.been.calledTwice;
      });
    }),
  );

  it('Lets you add behaviors at a later stage', () => {
    withWindow(async ({ document, javastick }) => {
      const later = Cypress.sinon.spy(
        (element) => (element.innerHTML = 'Later'),
      );
      document.body.innerHTML = '<div data-is="later"></div>';
      const attacher = javastick();

      attacher.install('later', later);

      await afterMicrotask(() => expect(later).to.have.been.called);
    });
  });
});

/**
 * Runs the give function after the current microtasks
 * and returns the result (unlike `queueMicrotask`).
 * This helps ensure assertions happen after MutationObserver's
 * microtasks have ran
 * @param {Function} fn
 * @returns Promise<Function>
 */
async function afterMicrotask(fn) {
  await Promise.resolve();
  return fn();
}

function withWindow(fn) {
  return function () {
    cy.window().then(fn);
  };
}
